/** @type {import('tailwindcss').Config}*/
const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],

  theme: {
    fontFamily: {
      recursia: ["RecursiaMDSNF", "monospace"],
    },
    colors: {
      bg: "#1c1e26",
      fg: "#b6c4f2",
      black: "#020203",
      red: "#e06c75",
      orange: "#d19a66",
      yellow: "#ffd88c",
      green: "#64d1a9",
      aqua: "#56b6c2",
      blue: "#56b6c2",
      purple: "#c678dd",
      white: "#b6c4f2"
    },
    extend: {}
  },

  plugins: []
};

module.exports = config;
